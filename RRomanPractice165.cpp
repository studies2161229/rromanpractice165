



#include <iostream>
#include <time.h>

int main()
{
	system("Color A");


	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);

	int DayNumber = buf.tm_mday;


	std::cout << "\n   " << "M A T R I X\n";
	const int N = 5;
	int array[N][N] = { 
		{0, 1, 2, 3, 4},
		{1, 2, 3, 4, 5},
		{2, 3, 4, 5, 6},
		{3, 4, 5, 6, 7},
		{4, 5, 6, 7, 8}, };
	std::cout << "\n" << "   ";



	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			std::cout << " ";
			std::cout << array[i][j];
		}
		std::cout << "\n" << "   ";
	}
	std::cout << "\n    " << "DayNumber % N = " << DayNumber % N << "\n";

	int Output = 0;
	for (int i = 0; i < N; i++) 
	{
		Output += array[DayNumber % N][i];
	}

	std::cout <<"\n    MATRIX row "<< DayNumber % N << " sum: " << Output << "\n\n\n";
	
}



